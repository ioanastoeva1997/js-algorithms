//Constants
const openBrackets = ['{', '[', '('];
const closingBrackets = ['}', ']', ')'];
const bracketPairMap = new Map([
    ['}', '{'],
    [']', '['],
    [')', '('],
]);

const areBracketsValid = (element, lastElementInArray) => {
    if (element === '{' && (lastElementInArray === '(' || lastElementInArray === '[')) return false;
    if (element === '[' && (lastElementInArray === '(')) return false;
    return true;
}


const addElement = (element, array) => {
    array.push(element);
}

const deleteElement = (array, element) => {
    if (array[array.length - 1] === bracketPairMap.get(element))
    array.pop();
}

//main function
const validateBrackets = (str) => {
    const bracketsArray = [];
    for (element of str) {
        if (openBrackets.includes(element)) {
            if (!areBracketsValid(element, bracketsArray[bracketsArray.length - 1])) {
                console.log(false);
                return;
            }
            addElement(element, bracketsArray);
        }
        if (closingBrackets.includes(element)) deleteElement(bracketsArray, element);
    };
    console.log(bracketsArray.length === 0);
}

validateBrackets('{asd}'); // true
validateBrackets('{[(asd)]}'); // true
validateBrackets('[{asd}]'); // false
validateBrackets('[(asd])'); // false
validateBrackets('{aaa[bbb(ccc)ddd]eee}'); // true
// other cases
validateBrackets('([asd])'); // false
validateBrackets('[(asd])'); // false 
validateBrackets('[(asd)]'); // true 
validateBrackets('({asd})'); // false
validateBrackets('{(asd})'); // false 

// optimized version
// const areBracketsValid1 =  (element, nextElement) => {
//     if ((element === '(' && nextElement === '{') || 
//     (element === '[' && (nextElement === '{')) || 
//     (element === '(' && (nextElement === '['))) 
//     return false;

//     return true;
// }

// const validateBrackets1 = (str) => {
    //     let isNotValid = true;
    //     const bracketsArray = [];
    //     let i = str.length-1; 
    
    //     for (const [index,element] of str.slice(0,Math.ceil(str.length/2)).split('').entries()) {
//         if (openBrackets.includes(element)) {
//             if (!areBracketsValid1(element, str[index+1])) {isNotValid = false; break;}
//             addElement(element, bracketsArray);
//         }
//         if (closingBrackets.includes(str[i])) deleteElement(bracketsArray, str[i]);
//         i--;
//     };
//     bracketsArray.length === 0 ? console.log(isNotValid) : console.log(false);
// }

// validateBrackets1('{asd}'); // true
// validateBrackets1('{[(asd)]}'); // true 
// validateBrackets1('[{asd}]'); // false
// validateBrackets1('[(asd])'); // false
// validateBrackets1('{aaa[bbb(ccc)ddd]eee}'); // true
// // other cases
// validateBrackets1('([asd])'); // false
// validateBrackets1('[(asd])'); // false 
// validateBrackets1('[(asd)]'); // true 
// validateBrackets1('({asd})'); // false
// validateBrackets1('{(asd})'); // false 