const primeDividers = (n) => {

    if (Number(n)) {
        for (let i = 1; i <= n; i++) {
            let isPrime = false;
            for (let j = 2; j < i; j++) {

                if (i % j === 0) {
                    isPrime = true;
                    break;
                }
            }
            if (i > 1 && isPrime === false && n % i === 0)
                console.log(i);
        }
    }
}
primeDividers(124);
